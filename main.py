from aiogram import Dispatcher, Bot, F
from aiogram.types import *
from aiogram.filters import Command, BaseFilter
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import StatesGroup, State
import asyncio
import aiosqlite
import suppress

import os
from dotenv import load_dotenv


load_dotenv()
BOT_TOKEN = os.getenv("BOT_TOKEN")
admin_id = [668695242]
dp = Dispatcher()

class AdminState(StatesGroup):
    newsletter = State()

class IsAdmin(BaseFilter):
    async def __call__(self, obj: TelegramObject) -> bool:
        return obj.from_user.id in admin_id
    
admin_keyboard = [
    [
        InlineKeyboardButton(text="Рассылка", callback_data='admin_newsletter'),
        InlineKeyboardButton(text="Статистика", callback_data='admin_statistic')
    ]
]
admin_keyboard = InlineKeyboardMarkup(inline_keyboard=admin_keyboard)

@dp.message(Command('admin'), IsAdmin())
async def admin_command(message: Message) -> None:
    await message.answer("Добро пожаловать в Админ-панель", reply_markup=admin_keyboard)

async def get_users():
    connect = await aiosqlite.connect('db')
    coursor = await connect.cursor()
    user_count = await coursor.execute('SELECT COUNT(*) FROM users')
    user_count = await user_count.fetchone()
    await coursor.close()
    await connect.close()
    return user_count[0]

async def get_all_users_id():
    connect = await aiosqlite.connect('db')
    coursor = await connect.cursor()
    all_ids = await coursor.execute('SELECT user_id FROM users')
    all_ids = await all_ids.fetchall()
    await coursor.close()
    await connect.close()
    return all_ids

@dp.callback_query(F.data == 'admin_statistic', IsAdmin())
async def admin_statistic(callback: CallbackQuery):
    stat_keyboard = [
        [InlineKeyboardButton(text="Вернуться", callback_data='menu')]
    ]
    stat_keyboard = InlineKeyboardMarkup(inline_keyboard=stat_keyboard)
    user_count = await get_users()
    await callback.message.edit_text('Статистика\n\n'
                                     f'Количество пользователей: {user_count}', reply_markup=stat_keyboard)
    
@dp.callback_query(F.data == 'admin_newsletter', IsAdmin())
async def admin_newsletter(callback: CallbackQuery, state: FSMContext):
    news_keyboard = [
        [InlineKeyboardButton(text="Вернуться", callback_data='menu')]
    ]
    news_keyboard = InlineKeyboardMarkup(inline_keyboard=news_keyboard)
    await callback.message.edit_text('Рассылка\n'
                                     '\nВведите сообщение, которое будет отправлено пользователям', reply_markup=news_keyboard)
    await state.set_state(AdminState.newsletter)

@dp.message(AdminState.newsletter)
async def admin_newsletter_step_2(message: Message, state: FSMContext):
    all_ids = await get_all_users_id()
    for user_id in all_ids:
        with suppress():
            await message.send_copy(user_id[0])
            await asyncio.sleep(0.3)
    await state.update_data(message_newsletter = message)
    await state.set_state(state=None)

@dp.callback_query(F.data == 'menu')
async def menu(callback: CallbackQuery):
    menu_keyboard = [
    [
        InlineKeyboardButton(text="Рассылка", callback_data='admin_newsletter'),
        InlineKeyboardButton(text="Статистика", callback_data='admin_statistic')
    ]
]
    keyboard = InlineKeyboardMarkup(inline_keyboard=menu_keyboard)
    await callback.message.edit_text("Добро пожаловать в Админ-панель", reply_markup=keyboard)

@dp.message(Command('start'))
async def hello(message: Message):
    start_keyboard = [
        [InlineKeyboardButton(text="create task", callback_data='create')],
        [InlineKeyboardButton(text="delete task", callback_data='delete')],
        [InlineKeyboardButton(text="show tasks", callback_data='show')]
    ]
    keyboard = InlineKeyboardMarkup(inline_keyboard=start_keyboard)
    await message.answer("What do u want?", reply_markup=keyboard)
    # await message.answer('Подождите, пожалуйста, я думаю...')
    # await asyncio.sleep(3)
    # await message.send_copy(message.from_user.id)

@dp.callback_query(F.data == 'create')
async def create_callback(callback: CallbackQuery):
    await callback.message.answer('enter task body')

@dp.callback_query(F.data == 'delete')
async def create_callback(callback: CallbackQuery):
    await callback.message.answer('which task u want to delete?')

@dp.callback_query(F.fdata == 'show')
async def show_callback(callback: CallbackQuery):
    await callback.message.answer('shows all tasks')

# @dp.message(F.text == 'Menu')
# async def menu(message: types.Message):
#     await message.answer('Menu')

async def main():
    bot = Bot(token = BOT_TOKEN)
    await dp.start_polling(bot)

if __name__ == '__main__':
    asyncio.run(main())